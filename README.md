# tera_thousands

## Simple filter for tera to split the numbers by thousands

### dependencies:
- [tera](https://crates.io/crates/tera)
- [thousands](https://crates.io/crates/thousands)

### Usage
The usage is simple:

First add this crate to the `Cargo.toml` file:
```Toml
tera_thousands = "0.1.0"
```

Now add the filter to your Tera instance:
```Rust
let mut tera = Tera::default();
tera.register_filter("separate_with_commas", tera_thousands::separate_with_commas);
```

You can now divide the numbers in your tera template with commas:
```Rust
let mut context = Context::new();
context.insert("number", &123456);

let output = tera
    .render_str("{{ number | separate_with_commas }}", &context)
    .expect("Expected a number");
assert_eq!(output, "123,456");
```

Also, you can use it with Rocket or any framework compatible with Tera.
For example, this is how it would be used with Rocket:
```
use rocket_dyn_templates::Template;
use tera_thousands::separate_with_commas;

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(Template::custom(|engines| {
            engines.tera.register_filter("separate_with_commas", separate_with_commas)
        }))
        .mount(...)
}
```

The possible options are:
- `separate_with_commas`
- `separate_with_dots`
- `separate_with_spaces`
- `separate_with_underscores`

### TO-DO
- [ ] An addition customizable filter from the template

Contributors are welcome :).
