use std::{collections::HashMap, hash::BuildHasher};
use tera::{to_value, Error, Value};
use thousands::Separable;

pub fn separate_with_commas<S: BuildHasher>(
    value: &Value,
    _: &HashMap<String, Value, S>,
) -> tera::Result<Value> {
    if value.is_f64() {
        Ok(to_value(value.as_f64().unwrap().separate_with_commas()).unwrap())
    } else if value.is_i64() {
        Ok(to_value(value.as_i64().unwrap().separate_with_commas()).unwrap())
    } else if value.is_u64() {
        Ok(to_value(value.as_u64().unwrap().separate_with_commas()).unwrap())
    } else {
        Err(Error::msg("Expected a number"))
    }
}

pub fn separate_with_spaces<S: BuildHasher>(
    value: &Value,
    _: &HashMap<String, Value, S>,
) -> tera::Result<Value> {
    if value.is_f64() {
        Ok(to_value(value.as_f64().unwrap().separate_with_spaces()).unwrap())
    } else if value.is_i64() {
        Ok(to_value(value.as_i64().unwrap().separate_with_spaces()).unwrap())
    } else if value.is_u64() {
        Ok(to_value(value.as_u64().unwrap().separate_with_spaces()).unwrap())
    } else {
        Err(Error::msg("Expected a number"))
    }
}

pub fn separate_with_dots<S: BuildHasher>(
    value: &Value,
    _: &HashMap<String, Value, S>,
) -> tera::Result<Value> {
    if value.is_f64() {
        Ok(to_value(value.as_f64().unwrap().separate_with_dots()).unwrap())
    } else if value.is_i64() {
        Ok(to_value(value.as_i64().unwrap().separate_with_dots()).unwrap())
    } else if value.is_u64() {
        Ok(to_value(value.as_u64().unwrap().separate_with_dots()).unwrap())
    } else {
        Err(Error::msg("Expected a number"))
    }
}

pub fn separate_with_underscores<S: BuildHasher>(
    value: &Value,
    _: &HashMap<String, Value, S>,
) -> tera::Result<Value> {
    if value.is_f64() {
        Ok(to_value(value.as_f64().unwrap().separate_with_underscores()).unwrap())
    } else if value.is_i64() {
        Ok(to_value(value.as_i64().unwrap().separate_with_underscores()).unwrap())
    } else if value.is_u64() {
        Ok(to_value(value.as_u64().unwrap().separate_with_underscores()).unwrap())
    } else {
        Err(Error::msg("Expected a number"))
    }
}

#[cfg(test)]
mod tests {
    use super::{
        separate_with_commas, separate_with_dots, separate_with_spaces, separate_with_underscores,
    };
    use tera::{Context, Tera};

    #[test]
    fn separate_with_commas_test() {
        let mut context = Context::new();
        context.insert("integer_number", &10000);
        context.insert("negative_number", &-69420);
        context.insert("float_number", &1234.567);

        let mut tera = Tera::default();
        tera.register_filter("separate_with_commas", separate_with_commas);

        let s_int = tera
            .render_str("{{ integer_number | separate_with_commas }}", &context)
            .expect("Expected a number");
        let s_neg = tera
            .render_str("{{ negative_number | separate_with_commas }}", &context)
            .expect("Expected a number");
        let s_flt = tera
            .render_str("{{ float_number | separate_with_commas }}", &context)
            .expect("Expected a number");

        assert_eq!(s_int, "10,000");
        assert_eq!(s_neg, "-69,420");
        assert_eq!(s_flt, "1,234.567");
    }

    #[test]
    fn separate_with_dots_test() {
        let mut context = Context::new();
        context.insert("integer_number", &10000);
        context.insert("negative_number", &-69420);
        context.insert("float_number", &1234.567);

        let mut tera = Tera::default();
        tera.register_filter("separate_with_dots", separate_with_dots);

        let s_int = tera
            .render_str("{{ integer_number | separate_with_dots }}", &context)
            .expect("Expected a number");
        let s_neg = tera
            .render_str("{{ negative_number | separate_with_dots }}", &context)
            .expect("Expected a number");
        let s_flt = tera
            .render_str("{{ float_number | separate_with_dots }}", &context)
            .expect("Expected a number");

        assert_eq!(s_int, "10.000");
        assert_eq!(s_neg, "-69.420");
        assert_eq!(s_flt, "1.234.567");
    }

    #[test]
    fn separate_with_spaces_test() {
        let mut context = Context::new();
        context.insert("integer_number", &10000);
        context.insert("negative_number", &-69420);
        context.insert("float_number", &1234.567);

        let mut tera = Tera::default();
        tera.register_filter("separate_with_spaces", separate_with_spaces);

        let s_int = tera
            .render_str("{{ integer_number | separate_with_spaces }}", &context)
            .expect("Expected a number");
        let s_neg = tera
            .render_str("{{ negative_number | separate_with_spaces }}", &context)
            .expect("Expected a number");
        let s_flt = tera
            .render_str("{{ float_number | separate_with_spaces }}", &context)
            .expect("Expected a number");

        assert_eq!(s_int, "10 000");
        assert_eq!(s_neg, "-69 420");
        assert_eq!(s_flt, "1 234.567");
    }

    #[test]
    fn separate_with_underscores_test() {
        let mut context = Context::new();
        context.insert("integer_number", &10000);
        context.insert("negative_number", &-69420);
        context.insert("float_number", &1234.567);

        let mut tera = Tera::default();
        tera.register_filter("separate_with_underscores", separate_with_underscores);

        let s_int = tera
            .render_str("{{ integer_number | separate_with_underscores }}", &context)
            .expect("Expected a number");
        let s_neg = tera
            .render_str(
                "{{ negative_number | separate_with_underscores }}",
                &context,
            )
            .expect("Expected a number");
        let s_flt = tera
            .render_str("{{ float_number | separate_with_underscores }}", &context)
            .expect("Expected a number");

        assert_eq!(s_int, "10_000");
        assert_eq!(s_neg, "-69_420");
        assert_eq!(s_flt, "1_234.567");
    }
}
